﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsimesedBlokid
{
    class Program
    {
        static void Main(string[] args)
        {
            // siit üles esialgu ära puutu

            // esimene keerukam lause - if

            Console.Write("Palju sa palka saad: ");
            int palk = int.Parse(Console.ReadLine());
            ;

            if (palk > 1000)
            {
                Console.WriteLine("sinuga võib täitsa kinno minna");
            }
            else if (palk > 500)
            {
                Console.WriteLine("lähme siis burgerisse");
            }
            else
            {
                Console.WriteLine("sinuga pole midagi peale hakata");
            }

            // if-i sõber switch

            switch(DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    Console.WriteLine("Täna teeme sauna");
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                    Console.WriteLine("Trenni asjad kaas");
                    goto default;
                case DayOfWeek.Sunday:
                    Console.WriteLine("Täna vedeleme kodus ja joome õlutit");
                    break;
                default:
                    Console.WriteLine("Täna peab tööle minema");
                    break;
            }

            DayOfWeek dw = DateTime.Now.DayOfWeek;
            if (dw == DayOfWeek.Saturday)
            {

            }
            else if (dw == DayOfWeek.Sunday)
            {

            }
            else if (dw == DayOfWeek.Wednesday || dw == DayOfWeek.Tuesday)
            {

            }
            else
            {

            }

            string misteha =
                (palk > 1000) ? "lähen sinuga kinno" :
             //   (palk > 700) ? "lähen sinuga hambburgerisse" :
                (palk > 500) ? "lähen sinuga parki jalutama" :
                "ei tee midagi";
            Console.WriteLine(misteha);

            // siit allapoole ära ka esialgu puutu
        }
    }
}
