﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KolmapäevaneÜlesanne2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("mis värv seal on / what color You see: ");
            string color = Console.ReadLine().ToLower();
            switch(color)
            {
                case "roheline":
                case "green":
                    Console.WriteLine("sõida rahulikult edasi");
                    break;
                case "kollane":
                case "yellow":
                    Console.WriteLine("oota kuni tuleb roheline");
                    break;
                case "punane":
                case "red":
                    Console.WriteLine("pean't pea - jää seisma");
                    break;
                default:
                    Console.WriteLine("pese silmad ja vaata veel");
                    break;
            }
        }
    }
}
