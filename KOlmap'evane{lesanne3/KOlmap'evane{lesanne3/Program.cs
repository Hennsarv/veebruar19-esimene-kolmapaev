﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KOlmap_evane_lesanne3
{
    class Program
    {
        static void Main(string[] args)
        {
            // küsime mitu õpilast meil on
            Console.Write("Mitu õpilast: ");
            int õpilasi = int.Parse(Console.ReadLine());

            // teeme kaks massiivi
            string[] nimed = new string[õpilasi];
            int[] vanused = new int[õpilasi];

            // hakkame küsima
            for (int i = 0; i < õpilasi; i++)
            {
                Console.WriteLine($"Õpilane {i+1}");
                Console.Write("nimi: "); nimed[i] = Console.ReadLine();
                Console.Write("vanus: "); vanused[i] = int.Parse(Console.ReadLine());
            }

            // kontrolliks trükime välja

            for (int i = 0; i < õpilasi; i++)
                Console.WriteLine($"{nimed[i]} vanusega {vanused[i]}");

            // arvutame keskmise vanuse - kuidas
            double summa = 0;
            foreach (var v in vanused) summa += v;
            double keskmine = summa / õpilasi;
            Console.WriteLine($"keskmine vanus on {keskmine}");

        }
    }
}
