﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FailistLugemine
{
    class Program
    {
        static void Main(string[] args)
        {
            //string failinimi = @"c:\henn\nimekiri.txt";
            string failinimi = @"..\..\Nimekiri.txt";
            string[] loetudread = File.ReadAllLines(failinimi);
            foreach (string rida in loetudread) Console.WriteLine(rida);

            string[] nimed = new string[loetudread.Length];
            int[] vanused = new int[loetudread.Length];



            for (int i = 0; i < loetudread.Length; i++)
            {
                string rida = loetudread[i];
                string[] osad = rida.Split(',');
                nimed[i] = osad[0];
                vanused[i] = int.Parse(osad[1].Trim());

            }

            double summa = 0;
            foreach (int x in vanused) summa += x;
            double keskmine = summa / vanused.Length;

            Console.WriteLine($"keskmine vanus on {keskmine}");




        }
    }
}
