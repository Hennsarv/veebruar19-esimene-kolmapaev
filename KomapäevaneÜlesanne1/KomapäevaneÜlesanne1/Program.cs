﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KomapäevaneÜlesanne1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("anna üks arv: ");
            int arvulineMuutuja = int.Parse(Console.ReadLine());
            Console.WriteLine("Arv on {0}", arvulineMuutuja);

            if (arvulineMuutuja % 2 == 0)
                Console.WriteLine("arv on paaris");
            else
                Console.WriteLine("arv on paaritu");

            Console.WriteLine(
                arvulineMuutuja % 2 == 0 ? "arv on paaris" : "arv on paaritu"
                );



        }
    }
}
