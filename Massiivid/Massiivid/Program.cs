﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {

            if (false)
            {
                Console.WriteLine("Mis su nimi on: ");

                string[] nimed = Console.ReadLine().Split(' ');
                Console.WriteLine("Eesnimi on sul {0}", nimed[0]);

                if (nimed.Length > 1)
                {
                    Console.WriteLine("Perenimi on sul {0}", nimed[1]);
                }

                int[][] arvud = { new int[] { 1, 2, 3 }, new int[] { 1, 2 }, new int[] { 3, 4, 5 } };
                Console.WriteLine(arvud.Length); // mis mulle öeldakse

                int[,] teised = { { 1, 2, 3, 4 }, { 4, 5, 6, 7 }, { 1, 4, 7, 38 } };
                Console.WriteLine(teised.Length); // mis mulle siin öeldakse
                Console.WriteLine(teised.Rank);
                Console.WriteLine(teised.GetLength(0));
                Console.WriteLine(teised.GetLength(1)); 
            }

            int[] mass = new int[10];
            ;

            for (int i = 0 ; i < mass.Length ; i++)
            {
                mass[i] = i * i;

            }

            foreach (int x in mass)
            {
                Console.WriteLine(x);
            }

            for (int i = 0; i < mass.Length; i++)
            {
                Console.WriteLine(mass[i]);
            }


        }
    }


}
