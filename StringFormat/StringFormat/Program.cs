﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFormat
{
    class Program
    {
        static void Main(string[] args)
        {
            string nimi = "Henn Sarv";
            int vanus = 63;

            //string vastus = nimi + " on " + vanus.ToString() + " aastat vana";
            //string vastus = String.Format("{0} on {1} aastat vana", nimi, vanus);
            //Console.WriteLine(vastus);

            //Console.WriteLine("{0} on {1} aastat vana", nimi, vanus);

            string vastus = $"{nimi} on varsti {vanus+1} aastat vana";
        }
    }
}
